const Rule = require('../mongo/mongoAuth');

const checkMatch = require('../lib/index');

var Router = require('koa-router');

const router = new Router({
  prefix: '/rules',
});

//Require ObjectId in order to access documents based on the _id
const ObjectId = require('mongodb').ObjectId;
const { search } = require('koa/lib/request');

// GET request
router.get('/', async (ctx, next) => {
  try {
    const result = await Rule.find({});
    ctx.body = result;
  } catch (e) {
    ctx.body = `${e.message}`;
  }
});

// GET rule by it's ID
router.get('/:id', async (ctx) => {
  const id = ctx.params.id;
  try {
    const result = await Rule.findOne({ _id: ObjectId(id) });
    if (!result) throw new Error(`ID ${id} not found in DB`);
    ctx.body = result;
  } catch (e) {
    ctx.body = `${e.message}`;
  }
});

//POST request
router.post('/', async (ctx) => {
  //Get the rules details from the body
  let { ruleName, regex } = ctx.request.body;
  console.log(ruleName);
  // console.log(data);
  try {
    const rule = new Rule({ ruleName, regex });
    rule.save();
    // If the rule is added successfully 200 status code is sent as the response
    ctx.response.status = 200;
    ctx.body = `new rule added\n${rule}`;
  } catch (e) {
    ctx.body = `${e.message}`;
  }
});

//Delete Request

router.delete('/:id', async (ctx) => {
  //Get the id from the url
  const id = ctx.params.id;
  try {
    const deleted = await Rule.deleteOne({ _id: ObjectId(id) });
    if (!deleted) throw new Error(`ID ${id} not found in DB`);
    ctx.body = `rule with ID:${id} DELETED`;
  } catch (e) {
    ctx.body = `${e.message}\nID ${id} NOT FOUND`;
  }
});

//Update request to update a specific product sent as the id
router.put('/:id', async (ctx) => {
  //Get the id from the url
  const id = ctx.params.id;
  //Get the rule details from the body
  let { ruleName, regex } = ctx.request.body;
  // console.log(obj);
  // console.log(`${ruleName}:${regex}`);
  try {
    const result = await Rule.findByIdAndUpdate(id, { ruleName, regex });
    ctx.response.status = 200;
    ctx.body = `rule with ID:${id} UPDATED`;
  } catch (e) {
    ctx.body = `${e.message}\nID ${id} NOT FOUND`;
  }
});

//  resolve endpoint to return an array of matching rules from the db
router.post('/resolve', async (ctx) => {
  const result = await Rule.find({});

  checkMatch(result, ctx);
});

//Export the router
module.exports = router;
