async function checkMatch(result, ctx) {
  let ruleArr = [];
  //   console.log(result);
  result.forEach((rule) => {
    let match = ctx.request.body.text.match(rule.regex);
    try {
      if (match) {
        // console.log(match);
        console.log(rule);
        ruleArr.push(rule);
      }
    } catch (e) {
      ctx.body = `${e.message}`;
    }
    ctx.body = ruleArr;
  });
}

module.exports = checkMatch;
