const { koaSwagger } = require('koa2-swagger-ui');

const yamljs = require('yamljs');

var Router = require('koa-router');

const docrouter = new Router({
  prefix: '/v1',
});


// .load loads file from root.
const spec = yamljs.load('./Docs/openapi.yaml');

// add router.use() for swegger
docrouter.use(koaSwagger({ swaggerOptions: { spec } }));

// add route for docs 
docrouter.get('/docs', koaSwagger({ routePrefix: false, swaggerOptions: { spec } }));

//Export the router
module.exports = docrouter;
