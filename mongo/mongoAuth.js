// mongo connection
const mongoose = require('mongoose');
// var config = require('../config');
// const uri =
//   'mongodb+srv://imrkhn03:pikachu25@cluster0.zmpwo.mongodb.net/regexRulesDb?retryWrites=true&w=majority';
const uri = process.env.URI;
console.log(uri);
try {
  // Connect to the MongoDB cluster
  mongoose.connect(
    uri,
    { useNewUrlParser: true, useUnifiedTopology: true },
    () => console.log(' Mongoose is connected')
  );
} catch (e) {
  console.log('could not connect');
}

// define rule SChema
const ruleSchema = new mongoose.Schema({
  ruleName: String,
  regex: String,
});

// define rule Model
const Rule = mongoose.model('regexRule', ruleSchema);

// Export mongoDB model
module.exports = Rule;
