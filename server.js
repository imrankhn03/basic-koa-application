var koa = require('koa');
const bodyParser = require('koa-bodyparser');

// var config = require('./config');

//Importing the routes
const ruleRoutes = require('./routes/rule.routes');

const docRoutes = require('./Docs')

var app = new koa();

//Using body parser
app.use(bodyParser());

//Registering the routes 
app.use(ruleRoutes.routes()).use(docRoutes.routes()).use(ruleRoutes.allowedMethods());

app.listen(`${process.env.PORT}`, function () {
  console.log(`Server running on http://127.0.0.1:${process.env.PORT}`);
  console.log(`Check API DOCS AT http://127.0.0.1:${process.env.PORT}/v1/docs`);
});
